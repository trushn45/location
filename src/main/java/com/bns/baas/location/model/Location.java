package com.bns.baas.location.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Created by nlre on 04/04/2017.
 */


@ApiModel(value = "Location")
@Data
@Accessors(chain = true)
public class Location {

    @ApiModelProperty(value = "The location id")
    @JsonProperty("city_id")
    private Long location_id;

    @ApiModelProperty(value = "The location name")
    private String name;

}
