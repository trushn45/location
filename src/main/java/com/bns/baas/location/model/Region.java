package com.bns.baas.location.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 * Created by nlre on 04/04/2017.
 */
public class Region {

    @ApiModelProperty(value = "The region id")
    @JsonProperty("region_id")
    private Long region_id;

    @ApiModelProperty(value = "The region name")
    private String name;

}
