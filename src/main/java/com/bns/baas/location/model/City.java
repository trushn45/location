package com.bns.baas.location.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * Created by nlre on 04/04/2017.
 */


@ApiModel(value = "City")
@Data
@Accessors(chain = true)
public class City {

    @ApiModelProperty(value = "The city id")
    @JsonProperty("city_id")
    private Long city_id;

    @ApiModelProperty(value = "The city name")
    private String name;

}
