package com.bns.baas.location.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;

/**
 * Created by nlre on 04/04/2017.
 */

@Configuration
public class RestConfiguration {
    @Bean
    public RestOperations restOperations() {
        return new RestTemplate();
    }
}
